import sys
import gi
import json
from os import listdir
from random import sample
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw
from src.player import Player
import logging

# Configure logging
logger = logging.getLogger("main")
logging.basicConfig(
    level = logging.DEBUG,
    format = "%(asctime)s - %(levelname)s - %(message)s",
    datefmt = "%Y-%m-%d %H:%M:%S"    
)

# Load the saved settings
current_files = listdir()
if ".config" not in current_files:
    logger.info("Creating empty configuration file.")
    with open(".config", "w") as file:
        json.dump({"library": None}, file)

def read_config():
    logger.info("Reading existing configuration file.")
    with open(".config") as file:
        CONFIG = json.load(file)
        logger.debug(CONFIG)
        return CONFIG

CONFIG = read_config()

# Instantiate global media player
player = Player()

def get_random_file(library, theme):
    """
    Return a random file from a given music theme folder.
    """
    files = listdir(library + "/" + theme)
    return sample(files, 1)[0]

def get_absolute_path(library, theme, file):
    """
    Return the absolute path of a music file, given the
    theme (folder) and filename.
    """
    return(library + "/" + theme + "/" + file)

class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.box1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.set_child(self.box1)

        self.set_default_size(600, 250)
        self.set_title("GMTools")

        # Create title bar
        self.header = Gtk.HeaderBar()
        self.set_titlebar(self.header)

        # Add button to select music folder/library
        self.music_library = Gtk.Button(label="Select music library")
        self.header.pack_start(self.music_library)

        # Define the music folder/library pop-up selector
        self.open_dialog = Gtk.FileChooserNative.new(
            title = "Select music library folder",
            parent = self,
            action = Gtk.FileChooserAction.SELECT_FOLDER
        )

        self.open_dialog.connect("response", self.open_response)
        self.music_library.connect("clicked", self.show_open_dialog)

        # Create volume slider
        self.volume_slider = Gtk.Scale()
        self.volume_slider.set_digits(0) # 0 decimal places
        self.volume_slider.set_range(0, 100)
        self.volume_slider.set_value(50)
        self.volume_slider.connect("value-changed", self.set_volume)
        self.box1.append(self.volume_slider)

        # Create a play button for each music folder (theme/setting)
        self.refresh_library()

        # Create a single global stop button
        self.button = Gtk.Button(label = "Stop")
        self.box1.append(self.button)
        self.button.connect("clicked", self.stop)

    def refresh_library(self):
        CONFIG = read_config()
        if CONFIG["library"] is not None:
            themes = listdir(CONFIG["library"])
            for theme in themes:
                self.button = Gtk.Button(label = "Play " + theme)
                self.button.set_margin_top(5)
                self.button.set_margin_bottom(5)
                self.button.set_margin_start(5)
                self.button.set_margin_end(5)
                self.box1.append(self.button)
                self.button.connect("clicked", self.play, theme)

    def show_open_dialog(self, button):
        self.open_dialog.show()

    def open_response(self, dialog, response):
        if response == Gtk.ResponseType.ACCEPT:
            file = dialog.get_file()
            folder = file.get_path()
            # Save the selected folder to the configuration file
            logger.info(
                "Saving library path " + folder + " to configuration file."
            )
            library = {"library": folder}
            with open(".config", "w") as file:
                json.dump(library, file)
            self.refresh_library()

    def set_volume(self, volume_slider):
        volume = int(volume_slider.get_value())
        player.set_volume(volume)

    def play(self, button, theme):

        # If media is already being played, then stop it by fading out so we
        # can start a new file. Also, get the current volume so we can return
        # to the value during the fade-in.
        if player.check_if_playing():   
            player.prefade_volume = player.get_volume()
            player.fade_out()
            player.stop()

        # Get the path to the library
        CONFIG = read_config()
        library = CONFIG["library"]

        # Choose a random file in the chosen theme, and pass it to the player.
        random_file = get_random_file(library, theme)
        print(random_file)
        path = get_absolute_path(library, theme, random_file)
        print(path)
        player.set_media(path)

        logger.info("Playing " + random_file + " from the " + theme + " theme.")
        
        player.fade_in()
        player.play()

    def stop(self, button):
        # Before stopping, we need to store the prefade volume so that we can
        # return to that volume when we start playing again. (Stopping the 
        # player turns the volume to zero.)
        player.prefade_volume = player.get_volume()
        player.stop()
        
class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()

app = MyApp(application_id="com.example.GMTools")
app.run(sys.argv)
