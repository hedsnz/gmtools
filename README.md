# GMTools

GMTools provides simple user interface for GMs of tabletop RPGs to set the
ambience of their game. Clicking on a theme (e.g., tavern, travel, forest, rain)
selects a random music file (supplied by the GM) within that particular theme.

GMTools is built with Python, Gtk and VLC.

## Setup

Developed on Pop!_OS 22.04 LTS and tested on Fedora Workstation 37.1;
installed on a Raspberry Pi 4.

Most dependencies are for Gtk, for which we follow
https://pygobject.readthedocs.io/en/latest/getting_started.html#ubuntu-getting-started.

Install all system dependencies (noting gtk-4.0 rather than 3.0):

```
# Debian-based
sudo apt update
sudo apt install libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-4.0 vlc

# Fedora
sudo dnf upgrade --refresh
sudo dnf install gtk4-devel gcc g++ clang cmake gobject-introspection-devel python3-pip vlc
```

Clone GMTools:

```
git clone https://gitlab.com/hedsnz/gmtools.git
cd gmtools
```

Create and activate Python virtual environment:

```
python3 -m venv env
source env/bin/activate
```

Install Python package dependencies:

```
pip3 install -r requirements.txt
```

## Usage

### Configuration

The application reads a JSON configuration file in the root project directory,
`config.json`. This currently has one key-value pair:

```
{
    "library": "path/to/music/library
}
```

If this file doesn't exist, it's created on app startup.

The `library` value should be set to the folder containing your music files.
This folder should further be split into several subfolders, each for a specific
genre/theme of music. These folders will become the buttons you can press to
play a random music file of a given theme.

For example, you may have folders that include "tavern", "travel", "combat",
and so on.

### Run application

In the activated virtual environment, run `python3 main.py`.

## References

- https://github.com/Taiko2k/GTK4PythonTutorial (tutorial that is responsible for some of the boilerplate Gtk code in this project)
- https://stackoverflow.com/questions/54335351/using-vlc-to-play-pause-an-audio-track-inside-of-separate-functions