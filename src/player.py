import vlc
from time import sleep
from logging import getLogger

logger = getLogger("main")

# Define the class for the music player, which will be globally available (so
# we only use one player, rather that a new player for each theme).
class Player:

    def __init__(self):
        self.player = vlc.MediaPlayer()
        # Define prefade_volume: the volume of media before fading for 
        # transitions. Used to ensure that we return to the same volume when we
        # fade in the next media track. Integer between 0 and 100 (i.e.,
        # percent). Defaults to 50%.
        self.prefade_volume = 50
        self.player.audio_set_volume(self.prefade_volume)

    def set_media(self, path):
        """Set the media attribute (i.e., song) of the player object."""
        media = vlc.Media(path)
        self.player.set_media(media)

    def check_if_playing(self):
        """Check if the player is currently playing media. Returns boolean."""
        if self.player is not None:
            if self.player.is_playing():
                logger.debug("Media is playing.")
                return True
            else:
                logger.debug("Media is not playing.")
                return False
        else:
            return False

    def set_volume(self, volume):
        logger.debug("Setting volume to " + str(volume))
        self.player.audio_set_volume(volume)

    def get_volume(self):
        volume = self.player.audio_get_volume()
        logger.debug("Getting volume: " + str(volume))
        return(volume)

    def play(self):
        self.player.play()

    def stop(self):
        if self.player.is_playing():
            logger.debug("Stopping music player.")
            self.player.stop()
        else:
            logger.debug("No media to stop.")

    def fade_out(self):
        """
        Fade out the volume to zero over 5 seconds in 0.5-second increments.
        """
        prefade_volume = self.get_volume()
        logger.debug("Fading out with prefade_volume = " + str(prefade_volume) +
            "...")
        # Then progressively reduce to zero over 5 seconds
        if not (prefade_volume <= 5):
            # Get the steps to reduce by. For example, if volume == 50, then
            # steps = 10, so each step reduces the volume by 10. 
            steps = int(prefade_volume / 10)
            # Reduce by a factor of each step every half a second.
            for i in range(1, 11):
                self.set_volume(prefade_volume - (i * steps))
                if i == 11:
                    logger.debug("Fading out completed.")
                else:
                    sleep(0.5)

    def fade_in(self):
        """
        Fade in the volume from zero to player.prefade_volume over 5 seconds in
        0.5-second increments.
        """
        # Check whether the
        # Check whether the volume is extremely low. 
        if self.prefade_volume < 5:
            self.prefade_volume = 5
        logger.debug("Fading in with prefade_volume = " +
            str(self.prefade_volume) + "...")
        steps = int(self.prefade_volume / 10)
        for i in range(1, 11):
            self.set_volume(i * steps)
            if i == 1:
                self.player.play()
            elif i in range(2, 10):
                sleep(0.5)
            else:
                logger.debug("Fading in completed.")
